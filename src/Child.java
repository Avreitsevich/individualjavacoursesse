public class Child {

    private String name;
    private String eyeColor;
    private int weight;

    public Child(String name, String eyeColor, int weight) {
        this.name = name;
        this.eyeColor = eyeColor;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public int getWeight() {
        return weight;
    }

}
