public class Man {
    private String name;
    private String nationality;
    private int age;
    private int height;
    private int weight;
    private boolean marriage;
    private Child child;

    public  Man(String name, String nationality, int age, int height, int weight, Child child){
         this.name = name;
         this.nationality = nationality;
        this.age = age;
        this.height = height;
        this.weight = weight;

        marriage = false;
    }

    public void marriage(){

    }

    public String getName() {
        return name;
    }

    public String getNationality() {
        return nationality;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isMarriage() {
        return marriage;
    }
}
