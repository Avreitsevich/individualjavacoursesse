package Student;

public class Student {
     private String name;
     private int age;
    private int course;
    private  int[] ratings;

    public Student(String name, int age, int course) {
        this.name = name;
        this.age = age;
        this.course = course;
        this.ratings = new int[0];
    }

    public Student(String name, int age, int course, int[] ratings) {
        this.name = name;
        this.age = age;
        this.course = course;
        this.ratings = ratings;
    }


    public int[] getRatings() {
        return ratings;
    }

    public void setRatings(int[] ratings) {
        this.ratings = ratings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public void show(){
        System.out.println("name = " + getName() + ", age = " + getAge() + ", course = " + getCourse());

    }

   int computeRate(){
        int average = 0;
        int y;
        for (y=0; y<this.ratings.length; y++) {
            average = average + this.ratings[y];
        }
        return average/this.ratings.length;
   }
}
